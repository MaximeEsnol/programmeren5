import React from 'react';
import Constants from 'expo';
import { StyleSheet, Text, View } from 'react-native';
import {Header, Icon, Input, Item} from "native-base";

class App {

  state = {
    input: ""
  };

  updateInput = (text) => {
    this.setState({input: text});
  };

  render(){
    return(
        <Header>
          <Header searchBar rounded>
            <Item>
              <Icon name={"ios-search"} onPress={() => this.findArtist()}/>
              <Input placeholder={"Search Pokemon"} onChangeText={(text) => this.updateInput(text)}>

              </Input>
            </Item>
          </Header>
        </Header>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});

export default App;