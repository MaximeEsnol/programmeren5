import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Landing from "./src/Landing";
import Search from "./src/Search";
import Pokemon from "./src/Pokemon";
import {Button, Footer, FooterTab, Icon, Container} from "native-base";

class App extends React.Component{

  state = {
    currentScreen: "home"
  };

  switchScreen = (currentScreen) => {
    this.setState({currentScreen: currentScreen});
  }

  renderScreen = () => {
    switch(this.state.currentScreen.split("?")[0]){
      case "home":
          return(
            <Landing switchScreen={this.switchScreen}/>
          );
        break;
      case "pokemon":
        return(
            <Pokemon pokemonIndex={this.state.currentScreen.split("?")[1]} switchScreen={this.switchScreen}/>
        )
        break;
      case "search":
          return(
            <Search switchScreen={this.switchScreen}/>
          )
        break;
    }
  }

  render(){
    return (
        <Container>
          {this.renderScreen()}

          <Footer>
              <FooterTab>
                  <Button onPress={() => this.switchScreen("home")}>
                      <Icon name={"ios-home"}/>
                  </Button>
              </FooterTab>
              <FooterTab>
                  <Button onPress={() => this.switchScreen("search")}>
                      <Icon name={"ios-search"}/>
                  </Button>
              </FooterTab>
          </Footer>
        </Container>
    );
  }
}

const globalStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export default App;
