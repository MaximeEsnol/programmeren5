import React from 'react';
import {Text, Button, StyleSheet, ImageBackground} from 'react-native';
import {Container} from 'native-base';

class Pokemon extends React.Component{

    pokemons = {
        pokemon:[
            {
                name: "Pikachu",
                power: "Elektrisch",
                evolve: "Raichu",
                weight: "6kg",
                image: "pikachu.png"
            },
            {
                name: "Pidgey",
                power: "Vliegend",
                evolve: "Pidgeotto",
                weight: "1.8kg",
                image: "pidgey.png"
            },
            {
                name: "Vulpix",
                power: "Vuur",
                evolve: "Ninetales",
                weight: "9.9kg",
                image: "vulpix.png"
            }
        ]
    };

    pokemon = this.pokemons.pokemon[this.props.pokemonIndex];

    render(){
        return(
            <Container>
                <Text style={style.h1}>{this.pokemon.name}</Text>
                <Text>Type: {this.pokemon.power}</Text>
                <Text>Evolueert in: {this.pokemon.evolve}</Text>
                <Text>Weegt: {this.pokemon.weight}</Text>

                <Button title={"Terug naar alle Pokemon"} onPress={() => this.props.switchScreen("home")}/>
            </Container>

        );
    }
}

const style = StyleSheet.create({
    h1: {
        fontSize: 48,
        fontWeight: "700"
    },
    image: {
        width: "100%",
        height: 240
    }
});

export default Pokemon;