import React from 'react';
import {ImageBackground, StyleSheet, ScrollView, Text} from 'react-native';
import {Container, ListItem, List} from 'native-base';

class Information extends React.Component {
    render() {
        return (
            <Container>
                <ImageBackground style={styles.bg}
                                 source={{uri: "http://pokemongolive.com/img/posts/raids_loading.png"}}>
                    <ScrollView style={styles.scrollable}>
                        <ScrollView horizontal={true} style={styles.scrollableHor} contentContainerStyle={{ paddingBottom: 120 }}>
                            <ImageBackground style={styles.mainImage}
                                             source={{uri: this.props.data.sprites.front_default}}>

                            </ImageBackground>

                            <ImageBackground style={styles.mainImage}
                                             source={{uri: this.props.data.sprites.back_default}}>

                            </ImageBackground>
                        </ScrollView>

                        <Text
                            style={styles.heading1}>{this.props.data.name.charAt(0).toUpperCase() + this.props.data.name.slice(1)}</Text>

                        <ListItem itemDivider>
                            <Text style={styles.heading2}>Size</Text>
                        </ListItem>

                        <ListItem>
                            <Text style={styles.label}>Height</Text>
                            <Text>{this.props.data.height}</Text>
                        </ListItem>
                        <ListItem>
                            <Text style={styles.label}>Weight</Text>
                            <Text>{this.props.data.weight}</Text>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text style={styles.heading2}>Abilities</Text>
                        </ListItem>
                        <List dataArray={this.props.data.abilities}
                              renderRow={(item) =>
                                <ListItem>
                                    <Text>{item.ability.name}</Text>
                                </ListItem>
                              }>
                        </List>
                    </ScrollView>
                </ImageBackground>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    bg: {
        width: "100%",
        height: "100%"
    },
    mainImage: {
        width: "100%",
        height: 250
    },
    scrollableHor: {
        height: 250,
        flexDirection: "row"
    },
    heading1: {
        fontSize: 42,
        fontWeight: "700",
        textAlign: "center"
    },
    heading2: {
        fontSize: 24,
        fontWeight: "500"
    },
    label: {
        fontWeight: "500"
    },
    scrollable: {
        backgroundColor: "rgba(255,255,255,0.75)"
    }
});

export default Information;