import React from 'react';
import {Image} from 'react-native';
import {Container} from 'native-base';

class Loading extends React.Component{
    render(){
        return(
          <Container>
            <Image style={{width: "100%", height: 250}} source={{uri: "https://media1.tenor.com/images/080ff34d3305888ca6e7e216fedc60dc/tenor.gif"}}/>
          </Container>
        );
    }
}

export default Loading;