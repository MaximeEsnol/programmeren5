import React from 'react';
import {StyleSheet, Text, View, ImageBackground, Image} from 'react-native';
import {Container, Header, Footer, Input, Item, Icon} from 'native-base';
import Information from "./Information";
import axios from 'axios';
import Loading from "./Loading";

class Search extends React.Component {

    searchDisabled = false;

    state = {
        data: {},
        input: "",
        onCall:true
    };

    findPokemon = () => {
        let self = this;
        let input = this.state.input;
        this.setState({onCall:true});

        if (!this.searchDisabled) {
            axios.get("https://pokeapi.co/api/v2/pokemon/" + input)
                .then((response) => {
                    self.setState({data: response.data, onCall:false});
                    this.searchDisabled = true;
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    };

    updateInput = (text) => {
        this.setState({input: text.toLowerCase()});
        this.searchDisabled = false;
    };

    renderBody = () => {
        if(this.state.onCall){
            return(<Loading/>)
        } else {
            return(<Information data={this.state.data}/>)
        }
    };

    render() {
        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <Icon name={"ios-search"} onPress={() => this.findPokemon()}/>
                        <Input placeholder={"Search Pokemon"} onChangeText={(text) => this.updateInput(text)}>

                        </Input>
                    </Item>
                </Header>

                {this.renderBody()}

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    heading1: {
        fontSize: 48,
        fontWeight: "700"
    },
    bg: {
        height: '100%',
        width: '100%'
    }
});

export default Search;