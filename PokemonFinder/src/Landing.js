import React from 'react';
import Constants from 'expo-constants';
import {StyleSheet, Text, View, ImageBackground, Button} from 'react-native';
import {Container, Header, Footer} from 'native-base';
import pokemons from '../App';

class Landing extends React.Component {
    render() {
        return (
            <Container styles={styles.container}>
                <Text style={styles.heading1}>
                    Pokemon Finder!
                </Text>

                <Container styles={styles.containerButton}>
                    <Button title={"Start searching"} onPress={() => this.props.switchScreen("search")}/>
                </Container>

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    heading1: {
        fontSize: 32,
        fontWeight: "700",
        textAlign: "center"
    },
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
        width: "100%"
    },
    containerButton: {
        position: "relative",
        maxWidth: "75%"
    }
});

export default Landing;