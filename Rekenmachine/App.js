import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

class App extends React.Component {
    state = {
        inputField: ""
    };

    clearInput = () => {
        this.setState({inputField: ""});
    };

    getAbsoluteValueOfResult = () => {
        this.setState({inputField: Math.abs(this.getCalculation())});
    };

    removeLastChar = () => {
        let currentInput = this.state.inputField;

        let length = currentInput.length;

        let newInput = currentInput.substr(0, length - 1);

        this.setState({inputField: newInput});
    };

    getCalculation = () => {
        let currentInput = this.state.inputField;
        let correctedInput = currentInput.replace("x", "*").replace(",", ".");
        return eval(correctedInput);
    };

    calculate = () => {
        this.setState({inputField: "" + this.getCalculation()});
    }

    appendToInput = (character) => {
        let currentInput = this.state.inputField;
        this.setState({inputField: currentInput + character});
    };

    render() {
        let rows = [["C", "ABS", "R", "/"], ["7", "8", "9", "x"], ["4", "5", "6", "-"], ["1", "2", "3", "+"], ["", "0", ",", "="]],
            numpPadRows = [];

        for (let row = 0; row < rows.length; row++) {
            let numPadSingleRow = [];

            for (let item = 0; item < rows[row].length; item++) {
                let current = rows[row][item],
                    onPressAction;

                if (current == "C") {
                    onPressAction = this.clearInput;
                } else if (current == "ABS") {
                    onPressAction = this.getAbsoluteValueOfResult;
                } else if (current == "R") {
                    onPressAction = this.removeLastChar;
                } else if (current == "=") {
                    onPressAction = this.calculate;
                } else {
                    onPressAction = () => {
                        this.appendToInput(current)
                    };
                }

                numPadSingleRow.push(
                    <TouchableOpacity style={styles.numPadButton} onPress={() => {
                        onPressAction()
                    }}>
                        <Text style={[styles.text, styles.numPadText]}>{current}</Text>
                    </TouchableOpacity>
                );
            }
            numpPadRows.push(
                <View style={styles.row}>
                    {numPadSingleRow}
                </View>
            );
        }

        return (
            <View style={styles.container}>
                <Text style={[styles.text, styles.input]}>
                    {this.state.inputField}
                </Text>
                <View style={styles.numpad}>
                    {numpPadRows}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        color: "#fff"
    },
    input: {
        fontSize: 32
    },
    numpad: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-around"
    },
    numPadButton: {
        backgroundColor: "#4a4a4a",
        borderRadius: 500,
        width: 64,
        height: 64,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 5,
        marginBottom: 5
    },
    numPadText: {
        fontSize: 22,
        fontWeight: "500"
    }
});

export default App;