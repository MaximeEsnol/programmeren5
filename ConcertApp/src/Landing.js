import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
    Container,
    Header,
    Left,
    Body,
    Right,
    Content,
    Footer,
    FooterTab,
    Button,
    Icon,
    Title,
    Subtitle, Card, CardItem
} from 'native-base';

class Landing extends React.Component{
    render(){
        return(
            <Container>
                <Content>
                    <Text style={styles.heading1}>For you</Text>

                    <Text style={styles.heading2}>You might like</Text>

                    <Card>
                        <CardItem header>
                            <Text>Concert name</Text>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Text>
                                    Some info about this concert and this artist.
                                </Text>
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>June 12th at 9:30pm, Ancienne Belgique, Brussels</Text>
                        </CardItem>
                    </Card>

                </Content>
                <Footer>
                    <FooterTab>
                        <Button active>
                            <Icon active name="home"/>
                        </Button>
                        <Button>
                            <Icon name="search"/>
                        </Button>
                        <Button>
                            <Icon name="people"/>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    heading1: {
        fontSize: 38,
        fontWeight: 600
    },
    heading2: {
        fontSize: 24,
        fontWeight: 500
    }
});

export default Landing;