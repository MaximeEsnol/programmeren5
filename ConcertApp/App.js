import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Container} from 'native-base';
import Landing from "./src/Landing";

class App extends React.Component {
    render() {
        return (
            <Container>
                <Landing/>
            </Container>

        );
    }
}

export default App;