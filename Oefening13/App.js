import React from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import Header from './components/Header';
import {Card, CardTitle, CardText} from './components/Card';
import IdentificationScreen from "./components/Identification";
import ContactScreen from "./components/ContactScreen";
import SocialScreen from "./components/SocialScreen";

class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Header/>
                <ScrollView contentContainerStyle={[ styles.scrollContainer]}>
                    <Card>
                        <CardTitle>Identification</CardTitle>

                        <IdentificationScreen/>
                    </Card>

                    <Card>
                        <CardTitle>Contact information</CardTitle>

                        <ContactScreen/>
                    </Card>

                    <Card>
                        <CardTitle>Social information</CardTitle>

                        <SocialScreen/>
                    </Card>

                    <Card>
                        <CardTitle>Allergies</CardTitle>
                    </Card>

                    <Card>
                        <CardTitle>Others</CardTitle>
                    </Card>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: "center"
    },
    scrollContainer: {
        alignItems: "center"
    }
});

export default App;
