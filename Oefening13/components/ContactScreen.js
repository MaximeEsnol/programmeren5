import React, {useState} from 'react';
import {View, StyleSheet, Alert, Picker, ActionSheetIOS, Button} from 'react-native';
import Input from "./Input";
import Expression from "../constants/Regex";
import GlobalButton from "./Button";
import LabeledText from "./LabeledText";

const ContactScreen = props => {

    const [address, setAddress] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');

    const [isConfirmed, setConfirmed] = useState(false);
    const [confirmedAddress, setConfirmedAddress] = useState('');
    const [confirmedEmail, setConfirmedEmail] = useState('');
    const [confirmedPhone, setConfirmedPhone] = useState('');

    const updateAddress = text => {
        setAddress(text);
    };

    const updateEmail = text => {
        setEmail(text);
    };

    const updatePhone = text => {
        setPhone(text.replace(Expression.PHONE, ""));
    };

    const confirmationHandler = () => {
        let isEmailValid = false, isPhoneValid = false, isAddressValid = false;

        if (email.match(Expression.EMAIL)) {
            isEmailValid = true;
        }

        if (phone.match(Expression.PHONE)) {
            isPhoneValid = true;
        }

        if(address.length > 0){
            isAddressValid = true;
        }

        if (isEmailValid && isPhoneValid && isAddressValid) {
            setConfirmedAddress(address);
            setConfirmedEmail(email);
            setConfirmedPhone(phone);
            setConfirmed(true);
        } else {
            let alertText = "Unable to confirm.\n";
            alertText += (isEmailValid) ? "" : "Email is invalid.\n";
            alertText += (isPhoneValid) ? "" : "Phone number is invalid.\n";
            alertText += (isAddressValid) ? "" : "Address is invalid.";
            Alert.alert(
                "Warning",
                alertText
            );
        }
    };

    const resetConfirmed = () => {
        setConfirmed(false);
        setConfirmedPhone('');
        setConfirmedEmail('');
        setConfirmedAddress('');
    };

    let output;

    if (isConfirmed) {
        output = (
            <View style={styles.container}>
                <LabeledText label={"Address"} text={confirmedAddress}/>

                <LabeledText label={"Email"} text={confirmedEmail}/>

                <LabeledText label={"Phone number"} text={confirmedPhone}/>

                <GlobalButton onPress={() => resetConfirmed()}>Edit</GlobalButton>
            </View>
        )
    } else {
        output = (
            <View style={styles.container}>
                <Input label={"Address"} onChangeText={(text) => updateAddress(text)} value={address}/>

                <Input label={"Email"} onChangeText={(text) => updateEmail(text)} value={email}/>

                <Input label={"Phone number"} onChangeText={(text) => updatePhone(text)} value={phone}/>

                <GlobalButton onPress={() => confirmationHandler()}>Confirm</GlobalButton>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            {output}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width: "100%",
        alignItems: "center"
    }
});

export default ContactScreen;