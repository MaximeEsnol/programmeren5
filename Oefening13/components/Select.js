import React, {useState} from 'react';
import {Picker, StyleSheet} from 'react-native';
import Color from "../constants/Colors";

const Select = props => {
    return(
        <Picker {...props} style={{...styles.picker, ...props.style}}>
            {props.children}
        </Picker>
    )
};

const styles = StyleSheet.create({
    picker: {
        borderWidth: 1,
        borderColor: Color.SECONDARY_LIGHTER,
        width: "100%",
        height: 75
    }
});

export default Select;