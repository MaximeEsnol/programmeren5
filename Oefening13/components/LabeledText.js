import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Color from "../constants/Colors";

const LabeledText = props => {
    return(
        <View style={styles.labelContainer}>
            <Text style={styles.label}>{props.label}</Text>
            <Text style={styles.content}>{props.text}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    labelContainer: {
        marginVertical: 5,
        flexDirection: "row",
        justifyContent: "flex-start",
        width: "100%",
        borderBottomWidth: 1,
        borderBottomColor: Color.SECONDARY_LIGHTER
    },

    label: {
        width: "50%",
        padding: 5,
        fontWeight: "700",
        fontSize: 16
    },

    content: {
        width: "50%",
        padding: 5,
        fontSize: 16
    }
});

export default LabeledText;