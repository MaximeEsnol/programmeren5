import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';

const LightButton = props => {

    return(
        <TouchableOpacity {...props} {{...styles.btn, ...props.style}}>
            <Text>{props.children}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    btn: {

    }
});

export default LightButton;