import React from 'react';
import {View, TextInput, StyleSheet, Text} from 'react-native';
import Color from "../constants/Colors";

const Input = props => {
    return(
        <View style={styles.inputContainer}>
            <Text style={{...styles.label, ...props.labelStyle}}>{props.label}</Text>
            <TextInput {...props} style={{...styles.input, ...props.style}}></TextInput>
        </View>

    );
};

const styles = StyleSheet.create({
    inputContainer: {
        width: "100%",
        marginVertical: 5,
    },

    input: {
        backgroundColor: "white",
        borderWidth: 1,
        borderColor: Color.SECONDARY_LIGHTER,
        width: "100%",
        paddingHorizontal: 10,
        paddingVertical: 5
    },

    label: {
        color: Color.SECONDARY_DAKER,
        fontSize: 14
    }
});

export default Input;
