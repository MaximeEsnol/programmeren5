import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Color from "../constants/Colors";

const Card = props => {
    return(
        <View {...props} style={{...styles.card, ...props.style}}>
            {props.children}
        </View>
    )
};

const CardTitle = props => {
    return(
        <Text {...props} style={{...styles.cardTitle, ...props.style}}>
            {props.children}
        </Text>
    )
};

const CardText = props => {
    return(
        <Text {...props} style={{...styles.cardText, ...props.style}}>
            {props.children}
        </Text>
    )
};

const styles = StyleSheet.create({
    card: {
        borderWidth: 4,
        borderColor: Color.SECONDARY,
        backgroundColor: "white",
        padding: 5,
        marginVertical: 10,
        width: 480,
        maxWidth: "95%",
        alignItems: "center",
        justifyContent: "center"
    },

    cardTitle: {
        fontSize: 20,
        borderBottomColor: Color.SECONDARY_LIGHTER,
        borderBottomWidth: 1,
        textAlign: "center",
        fontWeight: "600",
        color: Color.SECONDARY_DARKER
    },

    cardText: {
        fontSize: 16
    }
});

export {Card, CardTitle, CardText};