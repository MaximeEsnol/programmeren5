import React, {useState} from 'react';
import {View, StyleSheet, Alert, Picker, ActionSheetIOS, Button} from 'react-native';
import Input from "./Input";
import Expression from "../constants/Regex";
import GlobalButton from "./Button";
import LabeledText from "./LabeledText";

const SocialScreen = props => {

    const [facebook, setFacebook] =     useState('');
    const [twitter, setTwitter] =       useState('');
    const [instagram, setInstagram] =   useState('');

    const [isConfirmed, setConfirmed] =     useState(false);
    const [confirmedFB, setConfirmedFB] =   useState('');
    const [confirmedTw, setConfirmedTw] =   useState('');
    const [confirmedIg, setConfirmedIg] =   useState('');

    const updateTwitter = text => {
        if(text.substr(0, 1) != "@"){
            text = "@" + text;
        }
        setTwitter(text.replace(Expression.TWITTER, ""));
    };

    const updateInstagram = text => {
        setInstagram(text.replace(Expression.INSTAGRAM, ""));
    };

    const confirm = () => {
        if(facebook.length > 0 || twitter.length > 0 || instagram.length > 0){
            setConfirmed(true);
            setConfirmedFB(facebook);
            setConfirmedIg(instagram);
            setConfirmedTw(twitter);
        } else {
            Alert.alert(
                "Warning",
                "You have to enter at least one social media in order to confirm."
            );
        }
    };

    const reset = () => {
        setConfirmed(false);
        setConfirmedTw('');
        setConfirmedIg('');
        setConfirmedFB('');
    };

    let output;

    if(isConfirmed){
        output = (
            <View style={styles.container}>
                {(confirmedFB.length > 0) ? (<LabeledText label={"Facebook"} text={confirmedFB}/>) : null}

                {(confirmedTw.length > 0) ? (<LabeledText label={"Twitter"} text={confirmedTw}/>) : null}

                {(confirmedIg.length > 0) ? (<LabeledText label={"Instagram"} text={confirmedIg}/>) : null}

                <GlobalButton onPress={() => reset()}>Edit</GlobalButton>
            </View>
        )
    } else {
        output = (
            <View style={styles.container}>
                <Input label={"Facebook"} onChangeText={(text) => setFacebook(text)} value={facebook}/>

                <Input label={"Twitter"} onChangeText={(text) => updateTwitter(text)} value={twitter}/>

                <GlobalButton onPress={() => confirm()}>Confirm</GlobalButton>
            </View>
        )
    }

    return(
        <View style={styles.container}>
            {output}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width: "100%",
        alignItems: "center"
    }
});

export default SocialScreen;