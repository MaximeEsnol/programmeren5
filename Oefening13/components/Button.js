import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import Color from '../constants/Colors';

const GlobalButton = props => {
    return(
        <TouchableOpacity {...props} style={{...styles.button, ...props.style}}>
            <Text style={{...styles.text, ...props.textStyle}}>{props.children}</Text>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    button: {
        width: "75%",
        padding: 7,
        backgroundColor: Color.TERNARY,
        borderWidth: 3,
        borderColor: Color.TERNARY_DARKER,
        justifyContent: "center",
        alignItems: "center"
    },

    text: {
        fontSize: 17,
        color: "white",
        textAlign: "center"
    }
});

export default GlobalButton;