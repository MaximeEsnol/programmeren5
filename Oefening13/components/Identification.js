import React, {useState} from 'react';
import {View, StyleSheet, Alert, Picker, ActionSheetIOS, Button} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Input from "./Input";
import Expression from "../constants/Regex";
import GlobalButton from "./Button";
import LabeledText from "./LabeledText";
import Select from "./Select";

const IdentificationScreen = props => {

    const [firstName, setFirstName] =   useState('');
    const [lastName, setLastName] =     useState('');
    const [birthdate, setBirthdate] =   useState(new Date());
    const [bloodType, setBloodType] =   useState("none");

    const [show, setShow] =             useState(false);
    const [mode, setMode] =             useState('date');

    const [hasBeenConfirmed, setHasBeenConfirmed] =         useState(false);
    const [confirmedFirstName, setConfirmedFirstName] =     useState('');
    const [confirmedLastName, setConfirmedLastName] =       useState('');
    const [confirmedBirthdate, setConfirmedBirthdate] =     useState('');
    const [confirmedBloodType, setConfirmedBloodType] =     useState('');

    const firstNameInput = val => {
        setFirstName(val.replace(Expression.NAME, ""));
    };

    const lastNameInput = val => {
        setLastName(val.replace(Expression.NAME, ""));
    };

    const confirmationHandler = () => {
        if(firstName != "" && lastName != "" && bloodType != null){
            setHasBeenConfirmed(true);
            setConfirmedFirstName(firstName);
            setConfirmedLastName(lastName);
            setConfirmedBloodType(bloodType);
        } else {
            Alert.alert("Confirmation failed!", "Not all fields have been filled in.");
        }
    };

    const resetConfirmed = () => {
        let abort;

        Alert.alert("Warning",
            "You are about to edit your personal information. Do you want to proceed?",
            [
                {
                    text: "Yes",
                    onPress: () => {
                        setHasBeenConfirmed(false);
                        setConfirmedFirstName('');
                        setConfirmedLastName('');
                        setConfirmedBirthdate();
                        setConfirmedBloodType();
                    }
                },
                {
                    text: "No",
                    style: "cancel"
                }
            ],
            {
                cancelable: false
            });
    };

    const showActionSheet = () => {
        ActionSheetIOS.showActionSheetWithOptions(
            {
                options: ["A Positive", "A Negative", "B Positive", "B Negative", "AB Positive", "AB Negative", "O Positive", "O Negative", "Cancel", "Reset"],
                destructiveButtonIndex: 9,
                cancelButtonIndex: 8
            },
            buttonIndex => {
                if (buttonIndex === 0) {
                    setBloodType("A Positive");
                } else if (buttonIndex === 1) {
                    setBloodType("A Negative");
                } else if (buttonIndex === 2) {
                    setBloodType("B Positive");
                } else if (buttonIndex === 3){
                    setBloodType("B Negative");
                } else if (buttonIndex === 4){
                    setBloodType("AB Positive");
                } else if (buttonIndex === 5){
                    setBloodType("AB Negative");
                } else if (buttonIndex === 6){
                    setBloodType("O Positive");
                } else if(buttonIndex === 7){
                    setBloodType("O Negative");
                } else if(buttonIndex === 9){
                    setBloodType("none");
                }
            })
    };

    const getPickerOutput = () => {
        let output = "";

        if(Platform.OS == "android"){
            output = (
                <Select
                    selectedValue={bloodType}
                    style={styles.picker}
                    onSelectedValueChange={(selectedValue, selectedIndex) => setBloodType(selectedValue)}
                >
                    <Picker.Item label={"Select your blood type"} value={"none"}/>
                    <Picker.Item label={"A Positive"} value={"A Positive"}/>
                    <Picker.Item label={"A Negative"} value={"A Negative"}/>
                    <Picker.Item label={"B Positive"} value={"B Positive"}/>
                    <Picker.Item label={"B Negative"} value={"B Negative"}/>
                    <Picker.Item label={"AB Positive"} value={"AB Positive"}/>
                    <Picker.Item label={"AB Negative"} value={"AB Negative"}/>
                    <Picker.Item label={"O Positive"} value={"O Positive"}/>
                    <Picker.Item label={"O Negative"} value={"O Negative"}/>
                </Select>
            )
        } else {
            output = (
                <Button style={{marginHorizontal: 10}} title={(bloodType == "none") ? "Select your blood type" : bloodType}
                        onPress={() => showActionSheet()}
                ></Button>
            );
        }

        return output;
    };


    let output;

    if(hasBeenConfirmed){
        output = (
            <View style={styles.container}>
                <LabeledText label={"First name"} text={confirmedFirstName}></LabeledText>
                <LabeledText label={"Last name"} text={confirmedLastName}></LabeledText>
                <LabeledText label={"Blood type"} text={bloodType}></LabeledText>

                <GlobalButton onPress={() => resetConfirmed()}>Edit</GlobalButton>
            </View>
        )
    } else {

        output = (
            <View style={styles.container}>
                <Input label={"First name"} onChangeText={(text) => firstNameInput(text)} value={firstName}/>

                <Input label={"Last name"} onChangeText={(text) => lastNameInput(text)} value={lastName}/>



                {getPickerOutput()}

                <GlobalButton onPress={() => confirmationHandler()}>Confirm</GlobalButton>
            </View>
        );
    }

    return (
        <View style={styles.container}>
            {output}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width: "100%",
        alignItems: "center"
    },
    picker: {
        width: "100%",
        height: 50
    }
});

export default IdentificationScreen;
