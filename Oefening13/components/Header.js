import React from 'react';
import {StyleSheet, View, Platform, Text, StatusBar} from 'react-native';
import Color from "../constants/Colors";

const Header = props => {
    return(
        <View style={styles.header}>
            <Text style={styles.hdrText}>Contacts</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    header: {
        width: "100%",
        paddingTop: (Platform.OS === "ios") ? 40 : StatusBar.currentHeight,
        height: (Platform.OS === "ios") ? 90 : 50+StatusBar.currentHeight,
        backgroundColor: Color.PRIMARY,
        alignItems: "center",
        justifyContent: "center",
    },

    hdrText: {
        color: "white",
        fontWeight: "600",
        fontSize: 18
    }
});

export default Header;
