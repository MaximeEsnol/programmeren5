const Expression = {
    NAME:       /[^a-zA-Z-\s]/g,
    EMAIL:      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    PHONE:      /[^0-9]/,
    TWITTER:    /^[a-zA-Z0-9_]{1,15}$/,
    INSTAGRAM:  /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}$/
};

export default Expression;