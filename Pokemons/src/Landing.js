import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Container, Header, Footer} from 'native-base';

let background = require("../../kutapp/assets/poke-background.jpg");

class Landing extends React.Component {
    render() {
        return (
            <ImageBackground source={background} resizeMode="cover" style={bg}>
                <Text style={styles.heading1}>
                    Pokemon BlijfInUwKot!
                </Text>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    heading1: {
        fontSize: 24
    },
    bg: {
        height: '100%',
        width: '100%'
    }
});

export default Landing;