import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Landing from "./src/Landing";

class App extends React.Component{
  render(){
    return (
        <View style={globalStyles.container}>
          <Landing/>
        </View>
    );
  }
}

const globalStyles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export default App;
