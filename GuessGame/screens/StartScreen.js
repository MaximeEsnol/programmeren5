import React, {useState} from 'react';
import {Alert, ScrollView, View} from 'react-native';
import styles from "../constants/Styles";
import Card from "../components/Card";
import Colors from "../constants/Color";
import Title from "../components/Title";
import P from "../components/P";
import Input from "../components/Input";
import GlobalButton from "../components/GlobalButton";

const StartScreen = props => {
    const [enteredValue, setEnteredValue] = useState('');
    const [confirmed, setConfirmed] = useState(false);
    const [confirmedValue, setConfirmedValue] = useState();

    const changeHandler = text => {
        setEnteredValue(text.replace(/[^0-9]/g, ''));
    };

    const resetHandler = () => {
        setEnteredValue("");
        setConfirmed(false);
        setConfirmedValue();
    };

    const confirmHandler = () => {
        if(enteredValue != "" || !isNaN(enteredValue) || enteredValue < 1 || enteredValue > 100){
            setConfirmedValue(parseInt(enteredValue));
            setConfirmed(true);
        } else {
            Alert.alert(
                "Warning",
                "Please enter a valid number first."
            );
        }
    };

    let output;

    if(confirmed){
        output =
            <Card>
                <P>You entered: {confirmedValue}</P>
                <GlobalButton onPress={() => props.onStartGame(confirmedValue)} text={"Start guessing"}></GlobalButton>
            </Card>
    }

    return (
        <ScrollView contentContainerStyle={styles.container}>
            <Title>Guess Game</Title>
            <Card>
                <P>Insert a number</P>
                <Input maxLength={2} style={{width: 80}} keyboardType={"number-pad"} onChangeText={(text) => changeHandler(text)} value={enteredValue}/>
                <View style={styles.dualButton}>
                    <GlobalButton onPress={() => resetHandler()} color={Colors.SECONDARY_LIGHTER} >
                        Reset
                    </GlobalButton>
                    <GlobalButton onPress={() => confirmHandler()} color={Colors.SECONDARY_LIGHTER}>
                        Confirm
                    </GlobalButton>
                </View>
            </Card>

            {output}

        </ScrollView>
    )
};

export default StartScreen;