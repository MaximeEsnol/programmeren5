import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import Title from "../components/Title";
import styles from "../constants/Styles";
import Input from "../components/Input";
import Card from "../components/Card";
import P from "../components/P";
import GlobalButton from "../components/GlobalButton";
import Colors from "../constants/Color";

const Home = props => {

    const [name, setName] = useState('');
    const [confirmed, setConfirmed] = useState(false);
    const [confirmedName, setConfirmedName] = useState("");

    const inputHandler = value => {
        setName(value.replace(/[^a-zA-Z-]/g, ""));
    };

    const confirmHandler = () => {
        if(name != ""){
            setConfirmed(true);
            setConfirmedName(name);
        }
    };

    let output;

    if(confirmed){
        output = <Card><P>Hello, {confirmedName}. Thanks for playing.</P></Card>
    }

    return(
        <ScrollView contentContainerStyle={styles.container}>
            <Title>Home screen</Title>
            <Card>
                <P>Insert your name:</P>
                <Input value={name} onChangeText={(text) => inputHandler(text)}/>
                <GlobalButton onPress={() => confirmHandler()} color={Colors.PRIMARY} >
                    Start
                </GlobalButton>
            </Card>



            {output}
        </ScrollView>
    );
};

export default Home;