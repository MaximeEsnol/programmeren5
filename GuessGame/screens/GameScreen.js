import React, {useState, useRef} from 'react';
import {Alert, ScrollView, View} from 'react-native';
import styles from "../constants/Styles";
import Card from "../components/Card";
import Colors from "../constants/Color";
import Title from "../components/Title";
import P from "../components/P";
import Input from "../components/Input";
import GlobalButton from "../components/GlobalButton";

const DIRECTION_HIGHER = "higher";
const DIRECTION_LOWER = "lower";

const GameScreen = props => {

    let guess = (min, max, exclude = -1) => {
        min = Math.ceil(parseInt(min));
        max = Math.floor(parseInt(max));

        let valid = false, guessedValue;
        while (!valid) {
            guessedValue = Math.floor(Math.random() * (max - min)) + min;

            if (guessedValue != exclude && guessedValue > min && guessedValue < max) {
                valid = true;
            }
        }

        return guessedValue;
    };

    const initialGuess = guess(0, 100, props.userNumber);

    let [guessed, setGuessed] = useState(initialGuess);
    let [allGuesses, setAllGuesses] = useState([initialGuess]);
    let [lowestMax, setLowestMax] = useState(100);
    let [highestMin, setHighestMin] = useState(0);

    const nextGuessHandler = direction => {
        let newGuess;

        if (direction == DIRECTION_LOWER) {
            if(props.userNumber < guessed){
                if (lowestMax > guessed) {
                    setLowestMax(guessed);
                }
                newGuess = guess(highestMin, guessed, guessed);
            } else {
                Alert.alert("Wrong", "Check again");
            }

        } else if (direction == DIRECTION_HIGHER) {
            if(props.userNumber > guessed){
                if (highestMin < guessed) {
                    setHighestMin(guessed);
                }
                newGuess = guess(guessed, lowestMax, guessed);
            } else {
                Alert.alert("Wrong", "Check again");
            }
        }

        setGuessed(newGuess);
        setAllGuesses(pastGuesses => [...pastGuesses, newGuess]);
    };

    let output;

    if (guessed != props.userNumber) {

        output =
            <ScrollView contentContainerStyle={styles.container}>
                <Title>Guessing Screen</Title>
                <Card>
                    <P>Opponent's guess: {guessed}</P>

                    <View style={styles.dualButton}>
                        <GlobalButton onPress={() => nextGuessHandler(DIRECTION_HIGHER)}>
                            Higher
                        </GlobalButton>
                        <GlobalButton onPress={() => nextGuessHandler(DIRECTION_LOWER)}>
                            Lower
                        </GlobalButton>
                    </View>
                </Card>

                <Card>
                    <P>Psst... Your number is {props.userNumber}.</P>

                    <P>The highest guess was: {lowestMax}</P>
                    <P>The lowest guess was: {highestMin}</P>
                </Card>

                <Card>
                    {
                        allGuesses.map((value, index) => {
                            return(
                                <View>
                                    <P>#{index}: {value}</P>
                                </View>
                            )
                        })
                    }
                </Card>
            </ScrollView>
    } else {
        output =
            <View style={styles.container}>
                <Title>Game is Over!</Title>
                <Card>
                    <P>Won in {allGuesses.length} tries!</P>
                </Card>
            </View>
    }

    return (
        <View style={styles.container}>
            {output}
        </View>
    )
};

export default GameScreen;