import React, {useState} from 'react';
import {StyleSheet, Text, View, Alert} from 'react-native';
import {AppLoading} from "expo";
import * as Font from 'expo-font';
import {Ionicons} from '@expo/vector-icons';
import Header from "./components/Header";
import Home from "./screens/Home";
import StartScreen from './screens/StartScreen';
import NavButton from "./components/NavButton";
import Footer from "./components/Footer";
import GameScreen from "./screens/GameScreen";

export default function App() {
    const [screen, setScreen] = useState('homeScreen');
    const [selectedNumber, setSelectedNumber] = useState(0);
    const [dataLoaded, setDataLoaded] = useState(false);

    const gameHasStarted = (number) => {
        setSelectedNumber(number);
        setScreen("GameScreen");
    };

    let output;

    if (screen == "homeScreen") {
        output = <Home/>;
    } else if (screen == "startScreen") {
        output = <StartScreen onStartGame={gameHasStarted}/>;
    } else if (screen == "GameScreen") {
        output = <GameScreen userNumber={selectedNumber}/>;
    } else {
        output = <Home/>;
    }

    const fetchFonts = () => {
        return Font.loadAsync({
            "Raleway": require("./assets/Raleway-Regular.ttf")
        });
    };

    if(!dataLoaded){
        return(<AppLoading  startAsync={fetchFonts} onFinish={() => setDataLoaded(true)}/>)
    } else {
        return (
            <View style={styles.container}>
                <Header/>

                {output}

                <Footer>
                    <NavButton textStyle={styles.navText} changeTo={() => setScreen("homeScreen")}>
                        Home
                    </NavButton>
                    <NavButton textStyle={styles.navText} changeTo={() => setScreen("startScreen")}>
                        <Ionicons name={"logo-game-controller-b"} size={22} color={"white"}/>
                        Start
                    </NavButton>
                </Footer>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f7f7f7',
    },
    navText: {
        color: "white",
        fontFamily: "Raleway",
        fontSize: 16,
        flexDirection: "column"
    }
});
