import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Colors from "../constants/Color";

const Title = props => {
    return(
        <Text style={styles.title}>
            {props.children}
        </Text>
    );
};

const styles = StyleSheet.create({
    title: {
        fontSize: 24,
        fontFamily: "Raleway",
        color: Colors.PRIMARY_DARKER,
    }
});

export default Title;