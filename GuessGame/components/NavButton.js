import React from 'react';
import {StyleSheet, View, TouchableOpacity, Text} from 'react-native';

/**
 * Creates a navigational button.
 * @param props Properties of this component.
 * - changeTo: A function that handles changing the displayed screen.
 * - buttonStyle: Extra styles to be applied to the button
 * - textStyle: Extra styles to be applied to the text inside the button
 * @returns {*}
 * @constructor
 */
const NavButton = props => {

    return(
        <TouchableOpacity onPress={props.changeTo} style={{...styles.btn, ...props.buttonStyle}}>
            <Text style={{...styles.btnText, ...props.textStyle}}>
                {props.children}
            </Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    btn: {
        width: "50%",
        height: 40,
        alignItems: "center"
    }
});

export default NavButton;