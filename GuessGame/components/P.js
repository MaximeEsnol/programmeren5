import React from 'react';
import {StyleSheet, Text} from 'react-native';

const P = props => {
    return(
        <Text style={styles.p}>
            {props.children}
        </Text>
    )
};

const styles = StyleSheet.create({
    p: {
        fontSize: 18,
        fontFamily: "Raleway"
    }
});

export default P;