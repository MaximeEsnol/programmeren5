import React from 'react';
import {View, StyleSheet, Platform} from 'react-native';
import Colors from "../constants/Color";

const Footer = props => {
    return(
        <View style={{...styles.footer, ...props.style}}>
            {props.children}
        </View>
    );
};

const styles = StyleSheet.create({
    footer: {
        width: "100%",
        height: (Platform.OS === "ios") ? 80 : 65,
        justifyContent: "space-around",
        backgroundColor: Colors.PRIMARY,
        elevation: 1,
        flexDirection: "row",
        alignItems: "flex-end",
        paddingVertical: (Platform.OS === "ios") ? 20 : 5
    }
});

export default Footer;