import React from 'react';
import {StyleSheet, View} from 'react-native';

const Card = props => {

    return (
        <View style={{...styles.card, ...props.style}}>
            {props.children}
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        padding: 10,
        margin: 20,
        backgroundColor: "white",
        elevation: 1,
        width: 350,
        maxWidth: "95%",
        alignItems: "center",
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
    },
});

export default Card;