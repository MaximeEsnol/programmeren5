import React from 'react';
import {StyleSheet, TextInput} from 'react-native';
import Colors from "../constants/Color";

const Input = props => {

    return (
        <TextInput {...props} style={{...styles.input, ...props.style}}/>
    )
};

const styles = StyleSheet.create({
    input: {
        backgroundColor: "white",
        borderWidth: 2,
        borderRadius: 5,
        borderColor: Colors.BORDER_COLOR,
        padding: 8,
        width: "100%",
        marginVertical: 5,
        textAlign: "center"
    },
});

export default Input;