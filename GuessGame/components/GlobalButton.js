import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';
import Colors from "../constants/Color";

const GlobalButton = props => {

    return(
      <TouchableOpacity {...props} style={[{...styles.btn, ...props.style}]}>
          <Text style={{...styles.text, ...props.textStyle}}>
              {props.children}
          </Text>
      </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    btn: {
        width: "50%",
        height: 40,
        borderRadius: 10,
        margin: 5,
        backgroundColor: Colors.PRIMARY,
        alignItems: "center",
        justifyContent: "center"
    },
    text: {
        fontFamily: "Raleway",
        color: "white",
        fontSize: 16,
    }
});

export default GlobalButton;