import React from 'react';
import {View, Text, StyleSheet, StatusBar, Platform} from 'react-native';
import Colors from "../constants/Color";

const Header = (props) => {

    return(
        <View style={styles.header}>
            <Text style={styles.headerText}>GuessGame</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    header: {
        paddingTop: (Platform.OS == "ios") ? 48 : StatusBar.currentHeight,
        width: '100%',
        backgroundColor: Colors.PRIMARY,
        alignContent: 'flex-start',
        paddingBottom: 15,
        elevation: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
    },
    headerText: {
        color: 'white',
        fontWeight: '500',
        textAlign: 'center',
        fontSize: 18
    }
});

export default Header;