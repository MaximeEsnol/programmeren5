import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from "./Color";

const styles = StyleSheet.create({
    title: {
        fontSize: 22,
        color: Colors.PRIMARY_DARKER,
        fontWeight: "600"
    },
    container: {
        flex: 1,
        alignItems: "center",
        marginTop: 10
    },
    text: {
        fontSize: 18,
        marginVertical: 10
    },
    dualButton: {
        width: "60%",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 15,
        marginVertical: 10,
    },
    button: {
        padding: 10,
        margin: 10,
        borderRadius: 5,
        backgroundColor: Colors.PRIMARY,
        elevation: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    }
});

export default styles;