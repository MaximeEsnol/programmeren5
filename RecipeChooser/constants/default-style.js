import {StyleSheet} from 'react-native';
import Colors from '../constants/colors';

export default StyleSheet.create({
    mealTile: {
        width: "100%",
        aspectRatio: 16/9
    },
    mealImage: {
        width: 250,
        height: 50
    },
    mealContainer_Footer: {
        width: "100%",
    },
    mealFooter_Title: {
        width: "100%"
    },
    mealFooter_Info: {
        width: "100%"
    }
})
