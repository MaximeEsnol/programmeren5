import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from 'react-native';
import DefaultStyle from "../constants/default-style";
import { CommonActions } from '@react-navigation/native';

const CategoryGridTile = props => {

    return(
        <TouchableOpacity onPress={props.onPress} style={[styles.categoryItem, {backgroundColor: props.color}]} >
            <Text style={styles.categoryTitle}>{props.title}</Text>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    categoryItem: {
        backgroundColor: "white",
        borderRadius: 5,
        width: "45%",
        height: 100,
        margin: "2.5%",
        alignItems: "flex-end",
        justifyContent: "flex-end",
        padding: 20
    },
    categoryTitle: {
        fontSize: 18,
        fontWeight: "700"
    }
});

export default CategoryGridTile;