import React from 'react';
import DefaultStyle from "../constants/default-style";
import {ImageBackground, Text, View} from "react-native";

const MealTile = props => {
    return(
        <View style={DefaultStyle.mealTile}>
            <ImageBackground source={props.meal.image} style={DefaultStyle.mealImage} resizeMode={"cover"}>
                <View style={DefaultStyle.mealContainer_Footer}>
                    <View style={DefaultStyle.mealFooter_Title}>
                        <Text style={DefaultStyle.mealTitle}>
                            {props.meal.name}
                        </Text>
                    </View>
                    <View style={DefaultStyle.mealFooter_Info}>
                        <Text style={DefaultStyle.info_Text}>
                            {props.meal.duration}
                        </Text>
                        <Text style={DefaultStyle.info_Text}>
                            {props.meal.difficulty}
                        </Text>
                        <Text style={DefaultStyle.info_Text}>
                            {props.meal.price}
                        </Text>
                    </View>
                </View>
            </ImageBackground>
        </View>
    )
}

export default MealTile;