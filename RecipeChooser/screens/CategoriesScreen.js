import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, FlatList} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import CategoryGridTile from "../components/CategoryGridTile";

import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';
import CategoryMealScreen from "./CategoryMealScreen";

import {CATEGORIES} from "../data/dummy-data";

const goToDetails = (item) => {

};


const CategoriesScreen = props => {

    const renderGridItem = (itemData) => {
        return (
            <CategoryGridTile onPress={() => {
                props.navigation.navigate('CategoryMeal',
                    {
                        category: itemData.item
                    });
            }} title={itemData.item.title} color={itemData.item.color}/>
        )
    };

    return (
        <FlatList data={CATEGORIES} renderItem={renderGridItem} numColumns={2}/>
    );

};

const styles = StyleSheet.create({});

export default CategoriesScreen;
