import React from 'react';
import {StyleSheet, Text, View, ImageBackground} from 'react-native';

import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';
import {MEALS} from "../data/dummy-data";
import MealTile from "../components/MealTile";

const CategoryMealScreen = props => {
    props.navigation.setOptions({
        title: props.route.params.category.title,
        headerStyle: {
            backgroundColor: props.route.params.category.color
        },
        headerTintColor: "#ffffff"
    });

    const getMeals = () => {
        let category = props.route.params.category,
            id = category.id;

        let mealsToShow = findInCategory(id);

        console.log("meals in this: " + mealsToShow.length);

        return getOutputAll(mealsToShow);
    };

    const findInCategory = id => {
        let filteredMeals = [];

        for(let i = 0; i < MEALS.length; i++){
            if(mealHasCategory(MEALS[i], id)){
                filteredMeals.push(MEALS[i]);
            }
        }

        return filteredMeals;
    };

    const getOutputAll = meals => {
        let out = [];
        for(let i = 0; i < meals.length; i++){
            out.push(getSingleOutput(meals[i]));
        }
        return out;
    };

    const getSingleOutput = meal => {
        return (
            <View key={meal.id} style={DefaultStyle.mealContainer}>
                <MealTile meal={meal}/>
            </View>
        )
    };

    const mealHasCategory = (meal, category) => {
        return (meal.categories.includes(category));
    };

    return (
        <View styles={DefaultStyle.screen}>
                {getMeals()}
        </View>

    );

};

const styles = StyleSheet.create({});

export default CategoryMealScreen;
