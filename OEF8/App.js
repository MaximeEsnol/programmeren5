import React from 'react';
import {Constants} from 'expo-constants';
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Button,
    TouchableOpacity,
    StatusBar,
    ImageBackground,
    TextInput
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';


let shows = require("./data/shows");

function showHomeScreenShows() {
    let showElements = [];
    for (let i = 0; i < shows.shows.length; i++) {
        showElements.push((
            <TouchableOpacity style={styles.showContainer}>
                <Text numberOfLines={1} ellipsizeMode={'tail'} style={styles.title}>
                    {shows.shows[i].name}
                </Text>
                <Text style={styles.date}>
                    {shows.shows[i].dates[0].date} at {shows.shows[i].dates[0].doors}
                </Text>
            </TouchableOpacity>
        ));
    }

    return (
        <ScrollView horizontal={true} contentContainerStyle={styles.scrollContainer}>
            {showElements}
        </ScrollView>
    )
}

function HomeScreen({navigation}) {
    return (
        <ScrollView contentContainerStyle={styles.container}>
            <ImageBackground source={require("./assets/index.jpg")} resizeMode={"cover"}
                             style={[styles.background, styles.fromTop]}>
                <View style={styles.paddingTop}></View>
                <View style={styles.coverBackground}></View>
                <Text style={styles.heading2}>
                    For You
                </Text>

                {showHomeScreenShows({navigation})}
            </ImageBackground>
            <ImageBackground source={require("./assets/past.jpg")} resizeMode={"cover"} style={[styles.background]}>
                <View style={styles.coverBackground}></View>
                <Text style={styles.heading2}>
                    Past shows
                </Text>
                <Text style={styles.p}>
                    It seems you haven't been to any shows yet. You're missing out!
                    Once you went to a show, it will appear here.
                </Text>
            </ImageBackground>

        </ScrollView>

    )
}

function SearchScreen({navigation}) {
    return (
        <ScrollView contentContainerStyle={styles.container}>
            <View style={styles.search}>
                <Text style={styles.heading2}>
                    Search
                </Text>

                <View style={styles.searchContainer}>
                    <TextInput style={styles.searchInput}
                               placeholder={"Your next concert experience is a search away"}/>
                </View>
            </View>

        </ScrollView>
    )
}

function UpcomingShowsScreen({navigation}) {

    return(
        <ScrollView contentContainerStyle={styles.container}>
            <Text style={styles.heading2}>
                Upcoming
            </Text>
            {showHomeScreenShows()}
        </ScrollView>
    );
}

function ShowScreen({navigation}) {
    return (
        <View style={styles.container}>
            <ImageBackground style={[styles.background, styles.showInfoMain]} source={require("./assets/show.jpg")}
                             resizeMode={"cover"}>
                <View style={styles.coverBackground}></View>
                <Text style={styles.heading2}>
                    Show info
                </Text>
                <Text style={styles.p}>
                    When you click on a show, you'll see the details here.
                </Text>
            </ImageBackground>
        </View>
    )
}

function findShowById(id) {
    for (let i = 0; i < shows.shows.length; i++) {
        if (shows.shows[i].id == id) {
            return shows.shows[i];
        }
    }
    return false;
}

const Tab = createBottomTabNavigator();
export default function App() {
    return (
        <NavigationContainer style={styles.body}>
            <Tab.Navigator>
                <Tab.Screen name="Home" component={HomeScreen}/>
                <Tab.Screen name="Search" component={SearchScreen}/>
                <Tab.Screen name="Show" component={ShowScreen}/>
                <Tab.Screen name="Upcoming" component={UpcomingShowsScreen}/>
            </Tab.Navigator>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        backgroundColor: "#1a1a1a",
        paddingTop: Platform.OS == "android" ? StatusBar.currentHeight : 45
    },
    showInfoMain: {
        height: 200
    },
    darkContent: {
        position: "relative",
        flex: 1,
        padding: 5,
        backgroundColor: "#1a1a1a",
        width: "100%"
    },
    coverBackground: {
        backgroundColor: "#1a1a1a80",
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%"
    },
    coverBackgroundFromTop: {
        backgroundColor: "#1a1a1a80",
        position: "absolute",
        left: 0,
        width: "100%",
        height: "100%",
        top: StatusBar.currentHeight,
    },
    paddingTop: {
        backgroundColor: "#1a1a1a80",
        position: "absolute",
        left: 0,
        top: 0,
        height: StatusBar.currentHeight,
        width: "100%"
    },
    body: {
        backgroundColor: "#242424"
    },
    scrollContainer: {
        alignItems: "flex-start",
        padding: 10,
    },
    showContainer: {
        width: 150,
        backgroundColor: "#050505",
        height: 150,
        alignItems: "center",
        justifyContent: "flex-end",
        padding: 5,
        borderRadius: 5,
        elevation: 2
    },
    search: {
        backgroundColor: "#0d0d0d"
    },
    title: {
        color: "#fff",
        fontSize: 18,
        fontWeight: "500"
    },
    date: {
        color: "#fff"
    },
    heading2: {
        fontSize: 42,
        color: "#fff",
        fontWeight: "700",
        padding: 5
    },
    p: {
        color: "#fff",
        padding: 10,
        textAlign: "center"
    },
    searchContainer: {
        alignItems: "center",
        padding: 10
    },
    searchInput: {
        backgroundColor: "#fff",
        color: "#1a1a1a",
        padding: 10,
        width: "90%",
        borderRadius: 15
    }
});
