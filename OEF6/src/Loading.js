import React from 'react';
import {StyleSheet, Text, View, ImageBackground} from 'react-native';
import {Header, Icon, Input, Item, Container} from "native-base";
import {styles} from "../App";

class Loading extends React.Component{

    loadingGif = require("../assets/loading.gif");

    render() {
        return(
            <Container>
               <ImageBackground style={styles.loading} source={this.loadingGif}>

               </ImageBackground>
            </Container>
        )
    }
}

export default Loading;