import React from 'react';
import {StyleSheet, Text, View, ImageBackground, ScrollView} from 'react-native';
import {Header, Icon, Input, Item, Container, List, ListItem} from "native-base";
import {styles} from "../App";
import axios from 'axios';

class Results extends React.Component{

    results = this.props.data.results;

    render() {
        return(
            <ScrollView>
                <Text style={styles.heading1}>Search results for "{this.props.searchQuery}".</Text>

                <Container>
                    <List dataArray={this.results.artistmatches.artist} renderRow={(artist) =>
                        <ListItem>
                            <Text style={styles.heading3}>{artist.name}</Text>
                        </ListItem>}
                    >
                    </List>
                </Container>
            </ScrollView>
        )
    }
}

export default Results;