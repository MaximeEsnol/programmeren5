import React from 'react';
import {StyleSheet, Text, View, StatusBar, TouchableOpacity} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {SharedElement} from 'react-native-motion';


class App extends React.Component {

    animationRef;

    render() {
        return (
            <SharedElement id={"source"}>
                <View style={styles.container}>
                    <Animatable.Text style={styles.text} animation={"bounce"}
                                     direction={"alternate"}>Bounce</Animatable.Text>

                    <TouchableOpacity style={styles.button1} onPress={() => {
                        this._onPress();
                    }}>
                        <Animatable.View ref={(ref = "btn1") => this.animationRef = ref}>
                            <Text style={styles.button1Text}>Another animation</Text>
                        </Animatable.View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.button2} onPress={() => {
                        this._onPressBounceIn();
                    }}>
                        <Animatable.View ref={(ref = "btn2") => this.animationRef = ref}>
                            <Text style={styles.button2Text}>Bounce in animation</Text>
                        </Animatable.View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.button2} onPress={() => {
                        this._onPressFadeIn();
                    }}>
                        <Animatable.View ref={(ref = "btn2") => this.animationRef = ref}>
                            <Text style={styles.button2Text}>Bounce in animation</Text>
                        </Animatable.View>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.button2} onPress={() => {
                        this._onPressFlipInY();
                    }}>
                        <Animatable.View ref={(ref = "btn2") => this.animationRef = ref}>
                            <Text style={styles.button2Text}>Bounce in animation</Text>
                        </Animatable.View>
                    </TouchableOpacity>


                    <Animatable.Text style={styles.bigText} ref={(ref) => this.animationRef = ref}>
                        Animate me
                    </Animatable.Text>

                </View>
            </SharedElement>
        );
    }


    _onPress = () => {
        this.animationRef.bounce();
    }

    _onPressBounceIn = () => {
        this.animationRef.bounceIn();
    }

    _onPressFadeIn = () => {
        this.animationRef.fadeIn();
    }

    _onPressFlipInY = () => {
        this.animationRef.flipInY();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingTop: StatusBar.currentHeight
    },
    text: {
        fontSize: 20
    },
    button1: {
        backgroundColor: "#fcba03",
        width: "80%",
        padding: 20,
        alignItems: "center"
    },
    button2: {
        backgroundColor: "#32a852",
        width: "80%",
        padding: 20,
        alignItems: "center"
    },
    button2Text: {
        color: "#0d3b19"
    },
    button1Text: {
        color: "#574800",
    },
    bigText: {
        fontSize: 30
    }
});

export default App;
