import React from 'react';
import {StyleSheet, Text, View, ImageBackground, ScrollView} from 'react-native';
import {Header, Icon, Input, Item, Container, List, ListItem} from "native-base";
import {styles} from "../App";

class Results extends React.Component{

    results = this.props.data.results;

    render() {
        return(
            <ScrollView style={styles.container}>
                <Container>
                    <List dataArray={this.results.artistmatches.artist} renderRow={(artist) =>
                        <ListItem onPress={() => {this.props.getDetails(artist.name)}}>
                            <Text style={styles.heading3}>{artist.name}</Text>
                        </ListItem>}
                    >
                    </List>
                </Container>
            </ScrollView>
        )
    }
}

export default Results;