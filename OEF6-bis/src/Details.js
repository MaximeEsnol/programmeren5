import React from 'react';
import {StyleSheet, Text, View, ImageBackground, ScrollView} from 'react-native';
import {Header, Icon, Input, Item, Container, List, ListItem} from "native-base";
import {styles} from "../App";
import axios from 'axios';

const API_KEY = "8e3ffa7c3b797ef632b022f1d3939074";
const SECRET = "f2b3cc1fe7fe81a0dd0d7b2d86e617af";
const BASE_URL = "http://ws.audioscrobbler.com/2.0/?api_key=" + API_KEY + "&format=json&method=";

class Details extends React.Component{

    artist = this.props.artist.artist;

    state = {
        toptracts: {}
    };

    strip_html = (htmlText) => {
        return htmlText.split(/<[^>]*>/).join("");
    };

    displayArtistOnTour = () => {
        if(this.artist.ontour == 1){
            return (
                <Container style={styles.touring}>
                    <Text style={styles.p_touring}>
                        Currently on tour!
                    </Text>
                </Container>
            )
        }
    };

    getTopTracks = () => {
       // axios.get()
    };

    render() {
        return(
            <ScrollView>
                <ImageBackground style={styles.artistPic} source={{uri: this.artist.image[2]["#text"]}}>

                </ImageBackground>

                {this.displayArtistOnTour()}

                <Container style={styles.container}>
                    <Text style={styles.heading1}>{this.artist.name}</Text>
                        <Text style={styles.p}>{this.strip_html(this.artist.bio.summary)}</Text>
                    <Text style={styles.heading1}>Music</Text>
                </Container>

            </ScrollView>
        )
    }
}

export default Details;