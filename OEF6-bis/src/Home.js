import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Header, Icon, Input, Item, Container} from "native-base";
import {styles} from "../App";

class Home extends React.Component{

    render() {
        return(
            <Container>
                <Text style={styles.heading1}>ConcertApp</Text>

                <Text style={styles.p}>Start by typing the name of an artist.</Text>
            </Container>
        )
    }
}

export default Home;