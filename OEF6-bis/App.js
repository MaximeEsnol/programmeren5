import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Header, Icon, Input, Item, Container, FooterTab, Button, Footer} from "native-base";
import Home from "./src/Home";
import Loading from "./src/Loading";
import Results from './src/Results';
import Details from './src/Details';
import axios from 'axios';

const API_KEY = "8e3ffa7c3b797ef632b022f1d3939074";
const SECRET = "f2b3cc1fe7fe81a0dd0d7b2d86e617af";
const BASE_URL = "http://ws.audioscrobbler.com/2.0/?api_key=" + API_KEY + "&format=json&method=";

class App extends React.Component {

    state = {
        input: "",
        isLoading: false,
        artist: {},
        results: {},
        currentScreen: "home"
    };

    updateScreen = (newScreen) => {
        this.setState({currentScreen: newScreen});
    };

    renderScreen = () => {
        let current = this.state.currentScreen;

        if (this.state.isLoading) {
            return (<Loading/>)
        }

        if (current == "home") {
            return (<Home/>)
        } else if (current == "results") {
            return (<Results getDetails={this.getDetails} data={this.state.results}/>)
        } else if (current == "details") {
            return (<Details artist={this.state.artist}/>)
        }
    };

    findArtist = () => {
        let query = encodeURI(this.state.input);
        let self = this;

        this.startLoading();

        axios.get(BASE_URL + "artist.search&artist=" + query)
            .then((response) => {
                self.setState({results: response.data});
                this.stopLoading();
                self.updateScreen("results");
            })
            .catch((error) => console.log(error));
    };

    getDetails = (artistName) => {
        let name = encodeURI(artistName);
        let self = this;

        this.startLoading();

        axios.get(BASE_URL + "artist.getinfo&artist=" + name)
            .then((response) => {
                self.setState({artist: response.data});
                this.stopLoading();
                self.updateScreen("details");
            })
            .catch((error) => console.log(error));
    }

    updateInput = (text) => {
        this.setState({input: text});
    };

    startLoading = () => {
        this.setState({isLoading: true});
    }

    stopLoading = () => {
        this.setState({isLoading: false});
    }

    render() {
        return (
            <View>
                <Header searchBar rounded style={styles.header}>
                    <Item>
                        <Icon name={"ios-search"} onPress={() => this.findArtist()}/>
                        <Input placeholder={"Search an artist"} onChangeText={(text) => this.updateInput(text)}>

                        </Input>
                    </Item>
                </Header>

                {this.renderScreen()}

            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        margin: 5,
        padding: 5
    },
    header: {
        backgroundColor: "#491d75"
    },
    heading1: {
        fontSize: 48,
        textAlign: "center",
        fontWeight: "500"
    },
    p: {
        fontSize: 16
    },
    loading: {
        width: 200,
        height: 200,
        alignItems: "center",
        justifyContent: "center"

    },
    artistPic: {
        width: "100%",
        height: 250
    },
    touring: {
        backgroundColor: "#491d75",
        width: "100%",
        height: 50,
        alignItems: "center",
        justifyContent: "center"
    },
    p_touring: {
        color: "#fff",
        fontSize: 22
    }
});

export default App;
export {styles as styles};